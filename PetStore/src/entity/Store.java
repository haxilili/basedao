/*
 * 文件名：[Store.java]
 * 版权：〈哈西里里科技有限公司〉
 * 描述：〈描述〉
 * 修改人：〈修改人〉
 * 修改时间：2017年7月3日 下午3:10:04
 * 修改单号：〈修改单号〉
 * 修改内容：〈修改内容〉
 */

package entity;

/**
 * 商店类
 * 
 * @author meng
 *
 */
public class Store {

	private Integer id;
	private String name;
	private String password;
	private double balance;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

}
