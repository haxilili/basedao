/*
 * 文件名：[PetDao.java]
 * 版权：〈哈西里里科技有限公司〉
 * 描述：〈描述〉
 * 修改人：〈pc〉
 * 修改时间：2017年7月3日 下午4:00:11
 * 修改单号：〈修改单号〉
 * 修改内容：〈修改内容〉
 */

package dao;

import java.util.List;

import entity.Pet;

/**
 * 类名
 */
public interface PetDao {
	
	/**
	 * 查询所有宠物列表
	 * 
	 * @return
	 */
	List<Pet> findAllPets();

}
