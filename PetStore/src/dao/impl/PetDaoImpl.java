/*
 * 文件名：[PetDaoImpl.java]
 * 版权：〈哈西里里科技有限公司〉
 * 描述：〈描述〉
 * 修改人：〈pc〉
 * 修改时间：2017年7月3日 下午4:01:05
 * 修改单号：〈修改单号〉
 * 修改内容：〈修改内容〉
 */

package dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.haxilili.dao.BaseDao;
import com.haxilili.dao.Mapper;

import dao.PetDao;
import entity.Pet;

/**
 * 类名
 */
public class PetDaoImpl extends BaseDao implements PetDao {

	@Override
	public List<Pet> findAllPets() {

		String sql = "select * from pet";
		Mapper<Pet> mapper = new Mapper<Pet>() {

			@Override
			public Pet doMapper(ResultSet rs) {
				Pet pet = new Pet();
				try {
					pet.setId(rs.getInt("id"));
					pet.setBirthDay(rs.getDate("birthday"));
					pet.setHealth(rs.getInt("health"));
					pet.setLove(rs.getInt("love"));
					pet.setName(rs.getString("name"));
					pet.setOwnerId(rs.getInt("owner_id"));
					pet.setStoreId(rs.getInt("store_id"));
					pet.setTypeName(rs.getString("type_Name"));
					return pet;
				} catch (SQLException e) {
					e.printStackTrace();
				}
				return null;
			}
		};
		return super.query(sql, mapper);
	}

}
