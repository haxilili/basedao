/*
 * 文件名：[StoreDaoImpl.java]
 * 版权：〈哈西里里科技有限公司〉
 * 描述：〈描述〉
 * 修改人：〈pc〉
 * 修改时间：2017年7月3日 下午4:06:56
 * 修改单号：〈修改单号〉
 * 修改内容：〈修改内容〉
 */

package dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.haxilili.dao.BaseDao;
import com.haxilili.dao.Mapper;

import dao.StoreDao;
import entity.Store;

/**
 * 类名
 */
public class StoreDaoImpl extends BaseDao implements StoreDao {

	@Override
	public List<Store> findAllStore() {

		String sql = "select * from petstore";
		Mapper<Store> mapper = new Mapper<Store>() {

			@Override
			public Store doMapper(ResultSet rs) {
				Store s = new Store();
				try {
					s.setBalance(rs.getDouble("balance"));
					s.setId(rs.getInt("id"));
					s.setName(rs.getString("name"));
					s.setPassword(rs.getString("password"));
					return s;
				} catch (SQLException e) {
					e.printStackTrace();
				}
				return null;
			}
		};
		return super.query(sql, mapper);
	}

}
