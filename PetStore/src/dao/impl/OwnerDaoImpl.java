package dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.haxilili.dao.BaseDao;
import com.haxilili.dao.Mapper;

import dao.OwnerDao;
import entity.Master;

public class OwnerDaoImpl extends BaseDao implements OwnerDao {

	private Mapper<Master> mapper = new Mapper<Master>() {

		@Override
		public Master doMapper(ResultSet rs) {
			Master po = new Master();
			try {
				po.setId(rs.getInt("id"));
				po.setMoney(rs.getDouble("money"));
				po.setName(rs.getString("name"));
				po.setPassword(rs.getString("password"));
				return po;
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return null;
		}
	};

	@Override
	public Master findOwnerByName(String username) {
		String sql = "select * from petowner where name = ?";
		return queryOne(sql, mapper, username);
	}

	@Override
	public List<Master> findAllMasters() {
		String sql = "select * from petowner";
		return query(sql , mapper);
	}

}
