package dao;

import java.util.List;

import entity.Master;

public interface OwnerDao {

	/**
	 * 根据用户名查询一个用户
	 * 
	 * @param username
	 * @return
	 */
	Master findOwnerByName(String username);
	
	List<Master> findAllMasters();
	
	
}
