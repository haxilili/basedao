/*
 * 文件名：[StartFrame.java]
 * 版权：〈哈西里里科技有限公司〉
 * 描述：〈描述〉
 * 修改人：〈pc〉
 * 修改时间：2017年7月3日 下午4:17:40
 * 修改单号：〈修改单号〉
 * 修改内容：〈修改内容〉
 */

package ui;

import java.util.Scanner;

import biz.OwnerBiz;
import biz.StartBiz;
import biz.impl.OwnerBizImpl;
import biz.impl.StartBizImpl;
import entity.Master;
import entity.Pet;
import entity.Store;

/**
 * 类名
 */
public class StartFrame {
	
	
	
	public static void main(String[] args) {
		Scanner input =new Scanner(System.in);
		StartBiz biz = new StartBizImpl();
		OwnerBiz ownerBiz = new OwnerBizImpl();
		
		
		System.out.println("宠物商店启动");
		System.out.println("显式所有宠物");
		System.out.println("*********************************************");
		
		System.out.println("序号\t宠物名");
		for (Pet p : biz.findAllPet()) {
			System.out.println(p.getId()+"\t"+p.getName());
		}
		
		System.out.println("显式所有主人");
		System.out.println("*********************************************");
		for (Master p : biz.findAllMaster()) {
			System.out.println(p.getId()+"\t"+p.getName());
		}
		System.out.println("显式所有商店");
		System.out.println("*********************************************");
		for (Store p : biz.findAllStore()) {
			System.out.println(p.getId()+"\t"+p.getName());
		}
		
		
		System.out.println("请选择登录模式：1.宠物主人登录 2.宠物商店登录");
		int type = input.nextInt();
		
		if(type==1){
			Master po = new Master();
			System.out.println("请输入用户名：");
			po.setName(input.next());
			System.out.println("请输入密码：");
			po.setPassword(input.next());
			
			try {
				Master m = ownerBiz.login(po);
				System.out.println("登录成功");
				System.out.println("----------------------------");
				System.out.println("用户名："+m.getName());
				System.out.println("余额："+m.getMoney());
				System.out.println("用户名编号："+m.getId());
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}else{
			
		}
		
		
		
		
		
	}

}
