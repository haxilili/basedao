/*
 * 文件名：[StartBiz.java]
 * 版权：〈哈西里里科技有限公司〉
 * 描述：〈描述〉
 * 修改人：〈pc〉
 * 修改时间：2017年7月3日 下午4:09:26
 * 修改单号：〈修改单号〉
 * 修改内容：〈修改内容〉
 */

package biz;

import java.util.List;

import entity.Master;
import entity.Pet;
import entity.Store;

/**
 * 启动业务
 */
public interface StartBiz {

	List<Store> findAllStore();

	List<Pet> findAllPet();

	List<Master> findAllMaster();

}
