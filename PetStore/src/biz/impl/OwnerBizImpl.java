package biz.impl;

import biz.OwnerBiz;
import dao.OwnerDao;
import dao.impl.OwnerDaoImpl;
import entity.Master;

public class OwnerBizImpl implements OwnerBiz {
	
	
	private OwnerDao dao = new OwnerDaoImpl();

	@Override
	public Master login(Master po) {
		
		Master p = dao.findOwnerByName(po.getName());
		if(p==null)
			throw new RuntimeException(po.getName()+"用户名不存在");
		
		if(po.getPassword().equals(p.getPassword())){
			return p;
		}else{
			throw new RuntimeException("密码错误");
		}
	}

}
