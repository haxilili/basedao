/*
 * 文件名：[StartBizImpl.java]
 * 版权：〈哈西里里科技有限公司〉
 * 描述：〈描述〉
 * 修改人：〈pc〉
 * 修改时间：2017年7月3日 下午4:10:37
 * 修改单号：〈修改单号〉
 * 修改内容：〈修改内容〉
 */

package biz.impl;

import java.util.List;

import biz.StartBiz;
import dao.OwnerDao;
import dao.PetDao;
import dao.StoreDao;
import dao.impl.OwnerDaoImpl;
import dao.impl.PetDaoImpl;
import dao.impl.StoreDaoImpl;
import entity.Master;
import entity.Pet;
import entity.Store;

/**
 * 类名
 */
public class StartBizImpl implements StartBiz {

	private StoreDao storeDao = new StoreDaoImpl();
	private PetDao petDao = new PetDaoImpl();
	private OwnerDao ownerDao = new OwnerDaoImpl();

	@Override
	public List<Store> findAllStore() {
		return storeDao.findAllStore();
	}

	@Override
	public List<Pet> findAllPet() {
		return petDao.findAllPets();
	}

	@Override
	public List<Master> findAllMaster() {
		return ownerDao.findAllMasters();
	}

}
