/*
 * 作者：孟凡鼎
 */

package com.haxilili.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 通用版BaseDao
 * 
 * @author 孟凡鼎
 * @version 1.0
 *
 */
public class BaseDao {

	private static final String DRIVER = Messages.getString("driver");
	private static final String URL = Messages.getString("url");
	private static final String USERNAME = Messages.getString("username");
	private static final String PASSWORD = Messages.getString("password");

	private Connection connection;
	private PreparedStatement preparedStatement;
	private ResultSet rs;

	static {
		// 加载驱动
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			// System.exit(0);
		}

	}

	/**
	 * 打开连接
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Connection open() throws SQLException {
		Connection con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
		return con;
	}

	/**
	 * 关闭释放资源
	 */
	public void close() {
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
					preparedStatement = null;
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {
						if (connection != null) {
							connection.close();
							connection = null;
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}

			}
		}

	}

	/**
	 * 通用增删改
	 */
	public int updateDate(String sql, Object... objects) {
		try {
			Connection con = open();
			preparedStatement = con.prepareStatement(sql);

			if (objects != null)
				for (int i = 0; i < objects.length; i++) {
					preparedStatement.setObject(i + 1, objects[i]);
				}

			int i = preparedStatement.executeUpdate();
			return i;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return 0;

	}

	/**
	 * 通用查列表
	 */
	public <E> List<E> query(String sql, Mapper<E> mapper, Object... objects) {
		List<E> list;
		try {
			Connection con = open();
			preparedStatement = con.prepareStatement(sql);
			if (objects != null) {
				for (int i = 0; i < objects.length; i++) {
					preparedStatement.setObject(i + 1, objects[i]);
				}
			}
			rs = preparedStatement.executeQuery();

			list = new ArrayList<E>();

			while (rs.next()) {
				E e = mapper.doMapper(rs);
				list.add(e);
			}
			if (list.size() > 0)
				return list;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 通用查一个对象
	 * 
	 * @param sql
	 * @param mapper
	 * @param objects
	 * @return
	 */
	public <E> E queryOne(String sql, Mapper<E> mapper, Object... objects) {
		List<E> list = query(sql, mapper, objects);
		if (list != null && list.size() == 1)
			return list.get(0);
		return null;
	}

}
