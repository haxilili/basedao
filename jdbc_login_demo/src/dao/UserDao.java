/*
* 作者：孟凡鼎
*/

package dao;

import entity.User;

public interface UserDao {

	/**
	 * 保存
	 * 
	 * @param user
	 * @return
	 */
	int save(User user);
	
	/**
	 * 根据用户名查询出一个对象
	 * 
	 * @param name
	 * @return
	 */
	User findByName(String name);
	
}
