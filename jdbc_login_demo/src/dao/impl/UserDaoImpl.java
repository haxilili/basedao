/*
* ���ߣ��Ϸ���
*/

package dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.haxilili.dao.BaseDao;
import com.haxilili.dao.Mapper;

import dao.UserDao;
import entity.User;

public class UserDaoImpl extends BaseDao implements UserDao {

	private Mapper<User> mapper = new Mapper<User>() {

		@Override
		public User doMapper(ResultSet rs) {
			User u = new User();
			try {
				u.setuId(rs.getInt("uid"));
				u.setuName(rs.getString("uname"));
				u.setuPassword(rs.getString("upassword"));
				return u;
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return null;
		}
	};

	@Override
	public int save(User user) {
		return updateDate("insert into sys_user (uname,upassword) values (?,?)", user.getuName(), user.getuPassword());
	}

	@Override
	public User findByName(String name) {

		String sql = "select * from sys_user where uname = ?";
		return queryOne(sql, mapper, name);
	}

}
