/*
* 作者：孟凡鼎
*/

package entity;
/**
 * 实体类
 * pojo
 * 
 * @author pc
 *
 */
public class User {

	private Integer uId;
	private String uName;
	private String uPassword;

	public Integer getuId() {
		return uId;
	}

	public void setuId(Integer uId) {
		this.uId = uId;
	}

	public String getuName() {
		return uName;
	}

	public void setuName(String uName) {
		this.uName = uName;
	}

	public String getuPassword() {
		return uPassword;
	}

	public void setuPassword(String uPassword) {
		this.uPassword = uPassword;
	}

}
