/*
* 作者：孟凡鼎
*/

package biz.impl;

import biz.UserBiz;
import dao.UserDao;
import dao.impl.UserDaoImpl;
import entity.User;

public class UserBizImpl implements UserBiz {

	@Override
	public boolean register(User user) {
		UserDao dao = new UserDaoImpl();
		User u = dao.findByName(user.getuName());
		if (u != null)
			throw new RuntimeException(user.getuName() + ":用户已经存在！");
		dao.save(user);
		return true;
	}

}
